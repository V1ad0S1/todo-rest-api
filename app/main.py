from contextlib import asynccontextmanager
from fastapi import FastAPI

from app.api.api import api_router
from app.db import init as database_init
from app.core.settings import settings


@asynccontextmanager
async def lifespan(_: FastAPI):
    database_init()
    yield


app = FastAPI(title=settings.PROJECT_NAME, lifespan=lifespan)
app.include_router(api_router)


def main():
    import uvicorn

    uvicorn.run("app.main:app", reload=True, host=settings.DOMAIN, port=settings.PORT)


if __name__ == "__main__":
    main()
