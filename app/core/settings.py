from pydantic_settings import BaseSettings, SettingsConfigDict


class Base(BaseSettings):
    model_config = SettingsConfigDict(
        env_file=(".env", ".env.prod", ".env.local"),
        extra="ignore",
    )


class Settings(Base):
    PROJECT_NAME: str = "Test Project"

    DOMAIN: str = "localhost"
    PORT: int = 8000


class DatabaseSettings(Base):
    SQLITE_FILE_NAME: str = "todostore.db"
    ECHO: bool = True

    @property
    def url(self):
        return f"sqlite:///{self.SQLITE_FILE_NAME}"

    model_config = SettingsConfigDict(
        env_prefix="DB_",
    )


settings = Settings()
db_settings = DatabaseSettings()
