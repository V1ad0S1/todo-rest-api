from typing import Optional
from sqlmodel import SQLModel, Field


class TodoItemBase(SQLModel):
    title: str
    done: bool


class TodoItem(TodoItemBase, table=True):
    id: Optional[int] = Field(default=None, primary_key=True)
