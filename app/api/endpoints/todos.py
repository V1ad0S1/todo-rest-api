from fastapi import APIRouter, Depends, HTTPException
from sqlmodel import Session, select

from app.api.dependencies import get_session
from app.models.todos import TodoItem
from app.schemas.todos import TodoItemCreate, TodoItemResponse, TodoItemUpdate


router = APIRouter()


@router.get("/", response_model=list[TodoItemResponse])
def list_todos(session: Session = Depends(get_session)):
    todos = session.exec(select(TodoItem)).all()
    return todos


@router.post("/", response_model=TodoItemResponse)
def add_todo(todo: TodoItemCreate, session: Session = Depends(get_session)):
    db_item = TodoItem.model_validate(todo)
    session.add(db_item)
    session.commit()
    session.refresh(db_item)
    return db_item


@router.put("/{todo_id}", response_model=TodoItemResponse)
def edit_todo(todo_id: int, todo: TodoItemUpdate, session: Session = Depends(get_session)):
    todo_db = session.get(TodoItem, todo_id)
    if todo_db is None:
        raise HTTPException(status_code=404, detail=f"todo not found")
    updated_data = todo.model_dump(exclude_unset=True)
    todo_db.sqlmodel_update(updated_data)
    session.add(todo_db)
    session.commit()
    session.refresh(todo_db)
    return todo_db


@router.delete("/{todo_id}")
def delete_todo(todo_id: int, session: Session = Depends(get_session)):
    todo_db = session.get(TodoItem, todo_id)
    if todo_db is None:
        raise HTTPException(status_code=404, detail=f"todo not found")
    session.delete(todo_db)
    session.commit()
    return {"ok": True}
