from sqlmodel import SQLModel, create_engine

from app.core.settings import db_settings


connect_args = {"check_same_thread": False}
engine = create_engine(db_settings.url, echo=db_settings.ECHO, connect_args=connect_args)


def init():
    SQLModel.metadata.create_all(engine)
