from typing import Optional
from sqlmodel import SQLModel
from app.models.todos import TodoItemBase


class TodoItemResponse(TodoItemBase):
    id: int


class TodoItemCreate(TodoItemBase):
    done: bool = False


class TodoItemUpdate(SQLModel):
    title: Optional[str] = None
    done: Optional[bool] = None
